# ITV Checkout Test
A simple Checkout test for ITV.

## Build and Run
Navigate to project root

Run the following command

```
mvn clean install
```

This will generate a jar file in the target directory. Now we go to that directory and run the jar.

Run following commands
```
cd target
java -jar checkoutitvtest-1.0-jar-with-dependencies.jar
```

Alternatively you can run via Intelli J by navigating to Main.java. Then click on the green triangle on left hand side.
