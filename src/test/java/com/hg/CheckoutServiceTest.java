package com.hg;

import com.hg.checkoutitvtest.model.Discount;
import com.hg.checkoutitvtest.model.Item;
import com.hg.checkoutitvtest.service.CheckoutService;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class CheckoutServiceTest {

    public CheckoutService checkoutService;

    List<Item> items;
    Item a = new Item("A", 50);
    Item b = new Item("B", 30);
    Item c = new Item("C", 20);
    Item d = new Item("D", 15);

    @Before
    public void setUp() {
        checkoutService = new CheckoutService();

        Discount discountA = new Discount(3, 130);
        Discount discountB = new Discount(2, 45);

        a.setDiscount(discountA);
        b.setDiscount(discountB);
    }

    @Test
    public void calculatePriceNoDiscount() {
        items = new ArrayList<>();
        items.add(c);
        items.add(c);
        items.add(c);
        items.add(c);
        items.add(c);

        assertEquals(100, checkoutService.calculatePrice(items).intValue());
    }

    @Test
    public void calculatePriceDiscountForA() {
        items = new ArrayList<>();
        items.add(a);
        items.add(a);
        items.add(a);
        items.add(a);
        assertEquals(180, checkoutService.calculatePrice(items).intValue());
    }

    @Test
    public void calculatePriceDiscountForB() {
        items = new ArrayList<>();
        items.add(b);
        items.add(b);
        items.add(b);
        assertEquals(75,  checkoutService.calculatePrice(items).intValue() );
    }

    @Test
    public void calculatePriceDiscountForAAndB() {
        items = new ArrayList<>();
        items.add(b);
        items.add(a);
        items.add(b);
        assertEquals(95, checkoutService.calculatePrice(items).intValue());
    }

    @Test
    public void calculatePriceDiscountForMultipleItems() {
        items = new ArrayList<>();
        items.add(a);
        items.add(a);
        items.add(a);
        items.add(b);
        items.add(a);
        items.add(b);
        items.add(d);
        assertEquals(240, checkoutService.calculatePrice(items).intValue());
    }

}
