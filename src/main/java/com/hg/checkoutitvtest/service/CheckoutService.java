package com.hg.checkoutitvtest.service;

import com.hg.checkoutitvtest.model.Item;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CheckoutService {

    public Integer calculatePrice(List<Item> itemList) {

        int total = 0;

        // get unique count of each item
        Map<Item, Long> itemCounts = itemList.stream().collect(Collectors.groupingBy(e -> e, Collectors.counting()));

        for(Map.Entry<Item, Long> entry: itemCounts.entrySet()) {

            // if discount is null then calculate regular total
            if(entry.getKey().getDiscount() == null) {
                total += entry.getKey().getPrice() * entry.getValue().intValue();
            } else {
                // calculate how many times the discount is to be applied
                int discountOccurrence = entry.getValue().intValue() / entry.getKey().getDiscount().getQuantity();
                int nonDiscountOccurrence = entry.getValue().intValue() % entry.getKey().getDiscount().getQuantity();

                total += entry.getKey().getDiscount().getSpecialPrice() * discountOccurrence;

                // we might have a situation where some items meet the discount requirements but some dont so we want to
                // add these items at the regular price
                if(nonDiscountOccurrence != 0) {
                    total += entry.getKey().getPrice() * nonDiscountOccurrence;
                }
            }
        }

        return total;
    }
}
