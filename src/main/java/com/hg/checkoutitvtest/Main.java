package com.hg.checkoutitvtest;

import com.hg.checkoutitvtest.model.Discount;
import com.hg.checkoutitvtest.model.Item;
import com.hg.checkoutitvtest.service.CheckoutService;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Item a = new Item("A", 50);
        Item b = new Item("B", 30);
        Item c = new Item("C", 20);
        Item d = new Item("D", 15);
        Discount discountA = new Discount(3, 130);
        Discount discountB = new Discount(2, 45);

        a.setDiscount(discountA);
        b.setDiscount(discountB);

        List<Item> items = new ArrayList<>();

        items.add(b);
        items.add(a);
        items.add(b);

        CheckoutService checkoutService = new CheckoutService();
        System.out.println("total = " + checkoutService.calculatePrice(items));
    }
}
