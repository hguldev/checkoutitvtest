package com.hg.checkoutitvtest.model;

public class Discount {

    private Integer quantity;
    private Integer specialPrice;

    public Discount(Integer quantity, Integer specialPrice) {
        this.quantity = quantity;
        this.specialPrice = specialPrice;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getSpecialPrice() {
        return specialPrice;
    }

    public void setSpecialPrice(Integer specialPrice) {
        this.specialPrice = specialPrice;
    }
}
